from distutils.core import setup
from setuptools import find_packages

with open('requirements.txt') as f:
    requirements = f.read().splitlines()

setup(
    name='onbd',
    author='VK Energie',
    install_requires=requirements,
    packages=find_packages(),
    include_package_data=True,
    description='Python for praxis'
)
